﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlusMove : MonoBehaviour
{

    float valittuX;
    float valittuY;
    int nopeus;
    bool active;
    public Transform Alus;
    private Rigidbody2D rb2d;
    Vector3 pos;
    // Use this for initialization
    public Vector3 kohde;
    Vector2 dir;
    float step;
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        nopeus = 5;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            kohde = Camera.main.ScreenToWorldPoint(Input.mousePosition); ;
        }



        float step = nopeus * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, kohde, step);

        var dir = kohde - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        
		
		
    }
}
